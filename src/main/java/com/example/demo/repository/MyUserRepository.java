package com.example.demo.repository;

import com.example.demo.entity.MyUser;
import com.example.demo.entity.Sex;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MyUserRepository extends PagingAndSortingRepository<MyUser,Long> {

    Optional<MyUser> findFirstByEmail(String email);
    List<MyUser> findAllBySexOrderByEmailDesc(Sex sex);
    Page<MyUser> findAllByOrderById(Pageable page);
    List<MyUser> findBySex(Sex sex, Sort sort);

}
