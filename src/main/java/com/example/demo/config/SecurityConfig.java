package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

//    @Autowired
//    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("user1").password("1234").authorities("manager","user").and()
                .withUser("user2").password("abcd").authorities("user");

//        auth.jdbcAuthentication()
//                .dataSource(dataSource)
//                .passwordEncoder(myPasswordEncoder())
//                .usersByUsernameQuery("select name, password, 1 from my_user where name=?")
//                .authoritiesByUsernameQuery("select m.id, r.name from my_user m " +
//                        "join my_user_roles mr on mr.my_user_id=m.id " +
//                        "join user_role r on r.id=mr.roles_id " +
//                        "where m.name=?");

    }

    @Bean
    public PasswordEncoder myPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/myUsers").hasAuthority("user")
                .antMatchers("/myUsers/add").authenticated()
                .antMatchers("/myUsers/edit/*").hasAuthority("manager" +
                "")
        .and()
                .formLogin()
                .loginProcessingUrl("/zaloguj")
                .loginPage("/loginForm")
                .defaultSuccessUrl("/myUsers")
                .failureUrl("/loginFailure")
                .usernameParameter("username")
                .passwordParameter("password").and()
        .logout().logoutUrl("/wyloguj").logoutSuccessUrl("/myUsers").and()
        .exceptionHandling().accessDeniedPage("/accessDenied");
    }
}
