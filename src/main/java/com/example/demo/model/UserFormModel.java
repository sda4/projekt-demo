package com.example.demo.model;

import com.example.demo.entity.MyUser;
import com.example.demo.entity.Sex;
import com.example.demo.entity.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserFormModel {

    private Long id;

    @NotEmpty
    @Size(min=8, max=40)
    private String name;

    @NotEmpty
    @Size(max=40)
    private String firstName;

    @NotEmpty
    @Size(max=40)
    private String surname;

    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    @Size(min=4, max=20)
    private String password;

    private Long roleId;

    private List<UserRole> roles = new ArrayList<>();

    @NotNull
    private Sex sex;

    public UserFormModel(MyUser userEntity) {
        this.id = userEntity.getId();
        this.name = userEntity.getName();
        this.firstName = userEntity.getFirstName();
        this.surname = userEntity.getSurname();
        this.email = userEntity.getEmail();
        this.sex = userEntity.getSex();
    }
}
