package com.example.demo.controller;

import com.example.demo.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class EmailController {

    @Autowired
    private EmailService emailService;

    @GetMapping("/email")
    public String sendEmail() {
        emailService.sendEmail("szymon.dembek@gmail.com", "witaj SDA", "tresc");

        return "redirect:/myUsers";
    }
}
