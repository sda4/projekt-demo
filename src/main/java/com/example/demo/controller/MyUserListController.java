package com.example.demo.controller;

import com.example.demo.entity.MyUser;
import com.example.demo.entity.Sex;
import com.example.demo.entity.UserRole;
import com.example.demo.model.RoleSelectionModel;
import com.example.demo.model.UserFormModel;
import com.example.demo.repository.MyUserRepository;
import com.example.demo.repository.UserRoleRepository;
import com.example.demo.service.EmailService;
import com.example.demo.service.MyUserRolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Optional;

@Controller
@RequestMapping("/myUsers")
public class MyUserListController {

    @Autowired
    private MyUserRepository myUserRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private MyUserRolesService myUserRolesService;

    @Autowired
    private EmailService emailService;

    @GetMapping
    public String getUsers(Model model, @PageableDefault(size = 2)  Pageable page) {
        model.addAttribute("lista",  myUserRepository.findAllByOrderById(page));

        return "listaUserow";
    }

    @ModelAttribute("listaRol")
    public Iterable<UserRole> listaRol() {
        return userRoleRepository.findAll();
    }

    @ModelAttribute("listaPlci")
    public Sex[] listaPlci() {
        return Sex.values();
    }

    @GetMapping(path = "/add")
    public String addUser() {
        myUserRolesService.initSesja();

        return "redirect:/myUsers/userWithRoles";
    }

    @GetMapping("/userWithRoles")
    public String userWithRoles(Model model, Principal principal) {
        model.addAttribute("newRole", myUserRolesService.getRoleSelectionModel());
        model.addAttribute("newUser", myUserRolesService.getUserFormModel());
        emailService.bardzoDlugieZadanie();
        return "newUser";
    }

    @GetMapping(path="/edit/{userId}")
    public String editUser(@PathVariable("userId") MyUser user , Model model) {
//        MyUser user = myUserRepository.findOne(userId);

        if (user == null) {
            return "userNieIstnieje";
        }

        UserFormModel formModel = new UserFormModel(user);

        model.addAttribute("editedUser", formModel);

        return "editUser";
    }

    @GetMapping(path="/delete/{userId}")
    public String deleteUser(@PathVariable("userId") Long userId) {
        if (myUserRepository.findOne(userId) == null) {
            return "userNieIstnieje";
        }

        myUserRepository.delete(userId);

        return "redirect:/myUsers";
    }

    @PostMapping(path="/zapiszNowego")
    public String zapiszNowegUsera(@Validated @ModelAttribute("newUser") UserFormModel newUser,
                                 BindingResult bindingResult) {

        myUserRolesService.ustawDaneUseraNaSesji(newUser);

        Optional.ofNullable(newUser.getEmail())
                .flatMap(email -> myUserRepository.findFirstByEmail(newUser.getEmail()))
                .ifPresent(existingUser -> bindingResult.addError(new FieldError("newUser", "email",newUser.getEmail(),
                        false, new String[] {"emailExists"}, new Object[] {}, "User with this email already exists")));


        if (bindingResult.hasErrors()) {
            return "redirect:/myUsers/userWithRoles";
        }


        myUserRolesService.zapiszUsera();
        myUserRolesService.initSesja();

        return "redirect:/myUsers";
    }

    @PostMapping("/userWithRoles/addRole")
    public String dodajRoleDoUsera(RoleSelectionModel newRole) {
        myUserRolesService.ustawDaneRoliNaSesji(newRole);
        myUserRolesService.dodajRole();
        return "redirect:/myUsers/userWithRoles";
    }

    @GetMapping("/userWithRoles/removeRole")
    public String usunRoleOdUsera(@RequestParam("role") Integer nrRoli) {
        myUserRolesService.usunRole(nrRoli);

        return "redirect:/myUsers/userWithRoles";
    }

    @PostMapping(path="/zapiszIstniejacego/{userId}")
    public String zapiszEdytowanegoUsera(@PathVariable("userId") Long userId,
                                         UserFormModel editedUser, Principal principal) {

        MyUser entity = myUserRepository.findOne(userId);

        if (entity == null) {
            return "userNieIstnieje";
        }

        entity.setEmail(editedUser.getEmail());
        entity.setName(editedUser.getName());
        entity.setFirstName(editedUser.getFirstName());
        entity.setSurname(editedUser.getSurname());
        entity.setSex(editedUser.getSex());


        myUserRepository.save(entity);

        return "redirect:/myUsers";
    }
}
