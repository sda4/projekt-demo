package com.example.demo.controller;

import com.example.demo.service.LicznikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/licznik")
public class LicznikController {

    @Autowired
    private LicznikService licznikService;

    @GetMapping
    public String licznik(Model model) {
        model.addAttribute("wartosc", licznikService.getCurrentValue());

        return "licznik";
    }

    @PostMapping
    public String zwieksz() {
        licznikService.incrementValue();

        return  "redirect:/licznik";
    }
}
