package com.example.demo.service;

import com.example.demo.entity.MyUser;
import com.example.demo.model.RoleSelectionModel;
import com.example.demo.model.UserFormModel;
import com.example.demo.repository.MyUserRepository;
import com.example.demo.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION,
        proxyMode = ScopedProxyMode.TARGET_CLASS)
public class MyUserRolesService {

    private UserFormModel userFormModel;

    private RoleSelectionModel roleSelectionModel;

    @Autowired
    private MyUserRepository myUserRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @PostConstruct
    public void initSesja() {
        nowyUser();
    }

    public void nowyUser() {
        userFormModel = new UserFormModel();
        roleSelectionModel = new RoleSelectionModel();
    }

    public void ustawDaneUseraNaSesji(UserFormModel userFormModel) {
        this.userFormModel.setEmail(userFormModel.getEmail());
        this.userFormModel.setFirstName(userFormModel.getFirstName());
        this.userFormModel.setSurname(userFormModel.getSurname());
        this.userFormModel.setName(userFormModel.getName());
        this.userFormModel.setPassword(userFormModel.getPassword());
        this.userFormModel.setSex(userFormModel.getSex());
    }

    public void ustawDaneRoliNaSesji(RoleSelectionModel roleSelectionModel) {
        this.roleSelectionModel.setRoleId(roleSelectionModel.getRoleId());
    }

    @Transactional
    public void zapiszUsera() {
        MyUser entity = MyUser.builder()
                .name(userFormModel.getName())
                .firstName(userFormModel.getFirstName())
                .surname(userFormModel.getSurname())
                .email(userFormModel.getEmail())
                .password(passwordEncoder.encode(userFormModel.getPassword()))
                .sex(userFormModel.getSex())
                .build();

        entity.setRoles(userFormModel.getRoles());

        entity = myUserRepository.save(entity);
    }

    public void usunRole(int nrRoli) {
        userFormModel.getRoles().remove(nrRoli);
    }

    public void dodajRole() {
        if (roleSelectionModel.getRoleId() != null) {
            Optional.ofNullable(userRoleRepository.findOne(roleSelectionModel.getRoleId()))
                    .ifPresent(roleEntity -> userFormModel.getRoles().add(roleEntity));

        }
    }

    public RoleSelectionModel getRoleSelectionModel() {
        return roleSelectionModel;
    }

    public UserFormModel getUserFormModel() {
        return userFormModel;
    }
}
