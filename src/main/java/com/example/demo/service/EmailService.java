package com.example.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
@Slf4j
public class EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendEmail(String email, String subject, String message) {

       javaMailSender.send(new MimeMessagePreparator() {
           @Override
           public void prepare(MimeMessage mimeMessage) throws Exception {
               MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

               try {
                   helper.setTo(email);
                   helper.setFrom("abc@wp.pl", "kurs SDA");
                   helper.setSubject(subject);
                   helper.setText(message);
                   helper.setText("<html><body><b>tresc wiadomosci: </b>" + message + "</body></html>", true);
               } catch (MessagingException e) {
                   e.printStackTrace();
               }
           }
       });
    }

    @Scheduled(fixedRate = 2000)
    public void runScheduledTask() {
        log.error("Cykliczne odpalenie");
    }

    @Async
    public void bardzoDlugieZadanie() {
        log.error("przed sleepem");
        try {Thread.sleep(20000);

        } catch (InterruptedException e) {

        }

        log.error("po sleepie");
    }
}
