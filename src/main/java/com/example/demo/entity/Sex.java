package com.example.demo.entity;

public enum Sex {
    MALE("Mezczyzna"), FEMALE("Kobieta"), OTHER("Inna");

    private String name;

    Sex(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
