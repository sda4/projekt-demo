package com.example.demo.controller;

import com.example.demo.config.SecurityConfig;
import com.example.demo.entity.MyUser;
import com.example.demo.repository.MyUserRepository;
import com.example.demo.repository.UserRoleRepository;
import com.example.demo.service.MyUserRolesService;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;

@RunWith(SpringRunner.class)
@WebMvcTest(MyUserListController.class)
@Import({SecurityConfig.class})
public class TestMyUserControllerGetUser {

    @MockBean
    private MyUserRepository myUserRepository;

    @MockBean
    private UserRoleRepository userRoleRepository;

    @MockBean
    private MyUserRolesService myUserRolesService;

    @Autowired
    private MockMvc mvc;


    @Test
    public void testWithEmptyResult() throws Exception {
        //given
        BDDMockito.given(myUserRepository.findAll()).willReturn(Collections.emptyList());

        //when
        mvc.perform(MockMvcRequestBuilders.get("/myUsers")
            .with(SecurityMockMvcRequestPostProcessors.user("testowy").authorities(new SimpleGrantedAuthority("rola_1"))))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.view().name("listaUserow"))
            .andExpect(MockMvcResultMatchers.model().attribute("lista",
                    Matchers.emptyCollectionOf(MyUser.class
            )));

        //then
        BDDMockito.then(myUserRepository).should(Mockito.only()).findAll();
        Mockito.verifyNoMoreInteractions(myUserRepository);

        BDDMockito.then(userRoleRepository).should(Mockito.only()).findAll();
        Mockito.verifyNoMoreInteractions(userRoleRepository);

        Mockito.verifyZeroInteractions(myUserRolesService);
    }

    @Test
    public void testWithSingleElement() throws Exception {
        //given
        BDDMockito.given(myUserRepository.findAll()).willReturn(Collections.singleton((new MyUser())));
        //when
        mvc.perform(MockMvcRequestBuilders.get("/myUsers")
                .with(SecurityMockMvcRequestPostProcessors.user("testowy").authorities(new SimpleGrantedAuthority("rola_2"))))
                .andExpect(MockMvcResultMatchers.status().isForbidden());

        //then
    }

    @Test
    public void testWithSingleElementWithRedirect() throws Exception {
        //given
        BDDMockito.given(myUserRepository.findAll()).willReturn(Collections.singleton((new MyUser())));
        //when
        mvc.perform(MockMvcRequestBuilders.get("/myUsers"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection());
                //.andExpect(MockMvcResultMatchers.redirectedUrl(("/loginForm")));

        //then
    }
}
