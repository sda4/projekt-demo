package com.example.demo.controller;

import com.example.demo.entity.MyUser;
import com.example.demo.repository.MyUserRepository;
import com.example.demo.repository.UserRoleRepository;
import com.example.demo.service.MyUserRolesService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(MyUserListController.class)
@WithMockUser
public class TestMyUserControllerDeleteUser {

    @MockBean
    private MyUserRepository myUserRepository;

    @MockBean
    private UserRoleRepository userRoleRepository;

    @MockBean
    private MyUserRolesService myUserRolesService;

    @Autowired
    private MockMvc mvc;

    @Test
    public void testDeleteUserExists() throws Exception {
        BDDMockito.given(myUserRepository.findOne(15098L)).willReturn(new MyUser());

        mvc.perform(MockMvcRequestBuilders.get("/myUsers/delete/15098"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection())
                .andExpect(MockMvcResultMatchers.redirectedUrl("/myUsers"));
    }

    @Test
    public void testDeleteUserNotExists() throws Exception {
        BDDMockito.given(myUserRepository.findOne(34343L)).willReturn(null);

        mvc.perform(MockMvcRequestBuilders.get("/myUsers/delete/34343"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("userNieIstnieje"));

    }
}
